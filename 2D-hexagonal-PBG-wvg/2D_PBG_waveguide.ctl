(define-param DPML 1.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 1.) ; padding from wall

(define-param RADIUS 0.24)
(define-param EPSILON (* 1.95 1.95))
(define-param K_Z 0.0)  ; Out-of-plane component of the
(define-param excite-mode? false ) ; this is the default setting of this script

(define lattice-constant 1 )  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2))) )

(define-param NUM_CELL_1 4)
(define-param NUM_CELL_2 NUM_CELL_1 )
(define-param NUM_CELLS_WVG 0)

(define-param fcen 0.9)
(define-param df 0.2)
(define-param initial-src-center? false)

(define-param GRID_SIZE_X
    (* 2 (+
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y
    (* 2 (+
        (* NUM_CELL_2 (vector3-y basis2) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Z no-size )


; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))


(set! geometryList
  (geometric-objects-duplicates
    basis2 ; shift-vector
    (* -1 NUM_CELL_2) ; min-multiple
    NUM_CELL_2 ; max-multiple
    geometryList
  )
)

; remove part from parallelogram to make it hexagonal
; center
(define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_2 basis2)))
(define thicknessAirBlock
    (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1)
        (* 2 RADIUS)
    )
)

(set! geometryList (append geometryList
    (list
        (make block
            (center cAirBlock )
            (size thicknessAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        (make block
            (center (vector3* -1 cAirBlock ))
            (size thicknessAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        ; Waveguide hole
        (make cylinder
            (center 0 0 )
            (radius (+ RADIUS (* (vector3-x basis1) NUM_CELLS_WVG) ) )
            (height infinity)
            (material air)
        )
    )
    )
)


(set! geometry geometryList )

(set! resolution 20)
(set! pml-layers (list (make pml (thickness DPML))))

(define dir-name (string-append "2D_PBG_wvg-f-" (number->string fcen) "-df-" (number->string df)  ))

(if (not (= DPML 1.0))
  (set! dir-name (string-append dir-name "-pml-" (number->string DPML)  ) )
)
(if (not (= RADIUS 0.2))
  (set! dir-name (string-append dir-name "-r-" (number->string RADIUS)  ) )
)
(if (not (= NUM_CELL_1 4))
  (set! dir-name (string-append dir-name "-cells-" (number->string NUM_CELL_1)  ) )
)
(if excite-mode?
  (set! dir-name (string-append dir-name "-single" ) )
)
(use-output-directory dir-name)


(define srcCenter (vector3 (* RADIUS 1.1) RADIUS))
(if initial-src-center?
  (set! srcCenter (vector3 0 0))
)

(set! sources (list
  (make source
    (src (make gaussian-src
      (frequency fcen)
      (fwidth df)
      )
    )
    (component Ez)
    (center srcCenter )
    ; (size 1 1 )  
    )
  )
)

(run-sources+ 300
 (at-beginning output-epsilon)
 (after-sources (harminv Ez srcCenter fcen df))
)

(if excite-mode?
 (run-until (/ 1 fcen)
   (at-every (/ 1 fcen 20)  (output-png Ez "-Zc dkbluered -C eps-000000.00.h5") )
 )
)
