(define-param DPML 1.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 1.) ; padding from wall

(define-param RADIUS 0.2)
(define-param EPSILON (* 1.95 1.95))
(define-param MOUNTING_THICKNESS_REL 0.1 ) ; longitudinal thickness relative to longitudinal spatial period 'longitudinal-constant'
(define-param K_Z 0.0)  ; Out-of-plane component of the

(define lattice-constant 1 )  ; reference scale, a
(define longitudinal-constant (* 5 lattice-constant) ) ; longitudinal spatial period c, in terms of a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2) 0)) )
(define basis3 (vector3 0 0 longitudinal-constant ))

(define-param NUM_CELL_1 3)
(define-param NUM_CELL_2 NUM_CELL_1 )
(define-param NUM_CELL_LONGITUDINAL 4)

(define-param NUM_CELLS_WVG 0) ; number of cells of rods which will be removed in the center for the waveguide; 0 means the single center rod is removed


(define-param MOUNTING_RADIUS (* (+ NUM_CELL_1 1) lattice-constant ) ) ; radius of mounting plate is one cell larger than the rod lattice to make sure all rods are completely covered

; (define-param GRID_SIZE_X 10) ; useful size needed
(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y 
    (* 2 (+ 
        (* NUM_CELL_2 (vector3-y basis2) )
        PAD DPML
        )
    )
)
; Put mounting at the beginning and at the end of the rods
(define-param GRID_SIZE_Z 
    (* 2
        (+ ( * NUM_CELL_LONGITUDINAL longitudinal-constant)
        PAD DPML
        )
    )
)
; (NUM_CELL_LONGITUDINAL * longitudinal-constant )

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
(define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_2 basis2)))
(define thicknessAirBlock 
    (* (- (vector3-norm (vector3+ basis1 basis2)) RADIUS) 
        NUM_CELL_1
    )
)

(set! geometryList (append     
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis2 ; shift-vector
        (* -1 NUM_CELL_2) ; min-multiple
        NUM_CELL_2 ; max-multiple
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list 
        (make block 
            (center cAirBlock )
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        ; cut off parallelogram at lower bottom corner
        (make block 
            (center (vector3* -1 cAirBlock ))
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
     
    )
    )
)

; Periodic mounting 
(set! geometryList ( append geometryList
    (geometric-object-duplicates
      basis3 ; direction for the duplicates
      (* -1 NUM_CELL_LONGITUDINAL)  ; minimum of the multiples of the direction
      NUM_CELL_LONGITUDINAL ; maximum of the multiples of the direction
      (make cylinder  ; geometric object
        (center 0 0 0)
        (radius MOUNTING_RADIUS)
        (height (* MOUNTING_THICKNESS_REL longitudinal-constant ) )
        (material Glass)
      )
    )
  )  
)

(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 )
      (radius (+ RADIUS (* (vector3-x basis1) NUM_CELLS_WVG) ) )
      (height infinity)
      (material air)
    )
  )
  )
)

; --------------------------------------------------------------------
; --------------------------------------------------------------------
; --------------------------------------------------------------------



(set! geometry geometryList )
; (set! epsilon-input-file "mounted_2D_PBG_wvg-out/eps-000000.00.h5")
; (set! epsilon-input-file "excite-mounted_2D_wvg-f-0.292/eps-000000.00.h5" )
; (set! eps-averaging? false)
(set! resolution 20)
(set! pml-layers (list (make pml (thickness DPML))))


(use-output-directory) ; standard output directory
;(use-output-directory "excite-mounted_2D_wvg-f-0.197")


(define-param fcen 0.3)
(define-param df 0.4)
(define-param excite-mode? false)
(set! sources (list
               (make source
                 (src (make gaussian-src
                   (frequency fcen)
                   (fwidth df)
                   ))
                 (component Ez)
                 (center (* RADIUS 1.1) RADIUS 1))))

(run-sources+ 300
  (at-beginning output-epsilon)
  (after-sources (harminv Ez (vector3  (* RADIUS 1.1) RADIUS ) fcen df))
  )
(if excite-mode?
  (run-until (/ 1 fcen)
    (to-appended "ez" (at-every (/ 1 fcen 20) output-efield-z) ) 
    ; (at-every  output-efield-z)
  )
)

; (run-until 1 ( at-beginning output-epsilon))




