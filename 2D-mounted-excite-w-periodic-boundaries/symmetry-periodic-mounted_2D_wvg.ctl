#!/usr/local/bin/guile 
!#
(define-param DPML 1.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 4.) ; padding from wall

(define-param RADIUS 0.2)
(define-param EPSILON (* 1.95 1.95))
(define-param MOUNTING_THICKNESS_REL 0.1 ) ; longitudinal thickness relative to longitudinal spatial period 'longitudinal-constant'
(define-param K_Z 0.0)  ; Out-of-plane component of the

(define lattice-constant 1 )  ; reference scale, a
(define longitudinal-constant (* 5 lattice-constant) ) ; longitudinal spatial period c, in terms of a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2) 0)) )
(define basis3 (vector3 0 0 longitudinal-constant ))


(define-param fcen 0.3)
(define-param df 1.5)
(define-param initial-src-center? false)
(define-param only-eps? false)
(define-param eps-given? false)
(define-param excite-mode? false ) ; this is the default setting of this script

(define-param NUM_CELL_1 5)
(define-param NUM_CELL_2 NUM_CELL_1 )
(define-param NUM_CELL_LONGITUDINAL 1)

(define-param NUM_CELLS_WVG 0) ; number of cells of rods which will be removed in the center for the waveguide; 0 means the single center rod is removed


(define-param MOUNTING_RADIUS (* (+ NUM_CELL_1 1) lattice-constant ) ) ; radius of mounting plate is one cell larger than the rod lattice to make sure all rods are completely covered

; (define-param GRID_SIZE_X 10) ; useful size needed
(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y 
    (* 2 (+ 
        (* NUM_CELL_2 (vector3-y basis2) )
        PAD DPML
        )
    )
)
; Put mounting at the beginning and at the end of the rods
(define-param GRID_SIZE_Z 
    ( * (+ 1 NUM_CELL_LONGITUDINAL) longitudinal-constant)
)
; (NUM_CELL_LONGITUDINAL * longitudinal-constant )

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
; (define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_2 basis2)))
; (define thicknessAirBlock 
    ; (* (- (vector3-norm (vector3+ basis1 basis2)) RADIUS) 
        ; NUM_CELL_1
    ; )
; )
(define basis_1 basis1)
(define basis_2 basis2)
(define basis_3 basis3)

(define thicknessAirBlock
    (* (- (vector3-norm (vector3+ basis_1 basis_2)) RADIUS)
        NUM_CELL_1
    )
)
(define corner (vector3+
  (vector3* NUM_CELL_1 basis_1)
  (vector3* NUM_CELL_2 basis_2)
))

(define cAirBlock
  (vector3*
    corner
  (+ 0.75 (/
    (* 2.1 RADIUS)   ; a little bit more than a diameter
    (vector3-norm corner) ) )
  )
)


(set! geometryList (append
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis_2 ; shift-vector
        (* -1 NUM_CELL_2) ; min-multiple
        NUM_CELL_2 ; max-multiple
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list
        (make block
            (center cAirBlock )
            (size (* 0.5 thicknessAirBlock) thicknessAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) ) ; sum of basis_1 and basis_2
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
    ;    ; cut off parallelogram at lower bottom corner
        (make block
            (center (vector3* -1 cAirBlock ))
            (size (* 0.5 thicknessAirBlock) thicknessAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )

    )
  )
)

; (set! geometryList (append     
    ; ; Construct parallelogram of lattice cells
    ; (geometric-objects-duplicates
        ; basis2 ; shift-vector
        ; (* -1 NUM_CELL_2) ; min-multiple
        ; NUM_CELL_2 ; max-multiple
        ; geometryList
    ; )
    ; ; cut off parallelogram at upper right corner
    ; (list 
        ; (make block 
            ; (center cAirBlock )
            ; (size thicknessAirBlock infinity infinity)
            ; (e1 1.5 (/ (sqrt 3) 2) )
            ; (e2 -0.5 (/ (sqrt 3) 2) )
            ; (material air)
        ; )
        ; ; cut off parallelogram at lower bottom corner
        ; (make block 
            ; (center (vector3* -1 cAirBlock ))
            ; (size thicknessAirBlock infinity infinity)
            ; (e1 1.5 (/ (sqrt 3) 2) )
            ; (e2 -0.5 (/ (sqrt 3) 2) )
            ; (material air)
        ; )
     
    ; )
    ; )
; )

; Periodic mounting 
(set! geometryList ( append geometryList
    (geometric-object-duplicates
      basis3 ; direction for the duplicates
      (* -1 NUM_CELL_LONGITUDINAL)  ; minimum of the multiples of the direction
      0 ; maximum of the multiples of the direction
      (make cylinder  ; geometric object
        (center (vector3* 0.5 basis3) )
        ; (radius MOUNTING_RADIUS)
        (radius infinity)
        (height (* MOUNTING_THICKNESS_REL longitudinal-constant ) )
        (material Glass)
      )
    )
  )  
)

(print "Multiplicator of radius for waveguide:" (* (vector3-x basis1) NUM_CELLS_WVG) )

(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 )
      (radius (+ RADIUS (* (vector3-x basis1) NUM_CELLS_WVG) ) )
      (height infinity) 
      (material air)
    )
  )
  )
)

; --------------------------------------------------------------------
; --------------------------------------------------------------------
; --------------------------------------------------------------------

(if eps-given? 
  (begin
    (set! epsilon-input-file "excite-mounted_2D_wvg-eps-000000.00.res10.h5" )
    (set! eps-averaging? false)
    (set! pml-layers (list (make pml (thickness DPML))))
  )
  ; else
  (begin 
    (set! geometry geometryList )
    (set! pml-layers (list 
      (make pml (direction X) (thickness DPML) )
      (make pml (direction Y) (thickness DPML) )
      ; no pml in z direction
    ))
    )
)
; (set! eps-averaging? false)
(set! resolution 10)



(if not only-eps? 
  (use-output-directory) ; standard output directory
)
(use-output-directory (string-append "symmetry-periodic-mounted_2D_wvg-f-" (number->string fcen))
 ) 
; (define srcCenter (vector3 (* RADIUS 1.1) RADIUS ))
(define srcCenter (vector3 1.153 0.15 -1.07 ))
(if initial-src-center?
  (set! srcCenter (vector3 0 0 0))
)

(set! sources (list
               (make source
                 (src (make gaussian-src
                   (frequency fcen)
                   (fwidth df)
                   ))
                 (component Ez)
                 (center srcCenter )
                 )))

; ---- Explore symmetry ---
(set! symmetries (list 
  ;(make mirror-sym (direction Y))
  (make mirror-sym (direction X))
  )
)

(define kBrillouinBoundary (vector3* 
  (vector3 0 0 0.5)
  (/ 1 (+ NUM_CELL_LONGITUDINAL 1) longitudinal-constant )
) )



;(set! k-point kBrillouinBoundary)
; (set! k-point (vector3 0 0 0))      ; denomitated as k_0
(set! k-point 
  (vector3*
;   (vector3 0 0 0.3)     ; 1.6.17: checking mode at f=0.292 after scan over k . @ f= 0.292: k_z= 0.03 (in .out-file: freqs:, 13, ... )
    ; (vector3 0 0 0.425)   ; 5.6.17: checking different mode at f=0.47 from scan over k
    (vector3 0 0 0.225)     ; 5.6.17: different mode at f=0.288, based on result from scan @f=0.292
    (/ 1 (+ NUM_CELL_LONGITUDINAL 1) longitudinal-constant )
  )
)

; for scanning over k. Only used if scan-k?=true
(define k-points-list (list 
      (vector3 0 0 0)
      kBrillouinBoundary
 ))                 
(define-param k-interp 19)
(define-param scan-k? false)   ; default value is false

(if scan-k?
    (begin
     (run-k-points 600 (interpolate k-interp  k-points-list ))
     )
    ; else
    (begin
         (run-sources+ 600
          (at-beginning output-epsilon)
          (after-sources (harminv Ez srcCenter fcen df))
         )
        (run-until (/ 1 fcen)
          (to-appended "ez" (at-every (/ 1 fcen 20) output-efield-z) ) 
        ; (at-every  output-efield-z)
        )
     )
)
    



