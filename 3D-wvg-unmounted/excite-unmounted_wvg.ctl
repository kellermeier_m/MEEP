; 10.6.2017: Properties of this structure
;   compared to previous simulations the waveguide hole goes up to the beginning of the rods 
;   so the hole radius is r_hole = lattice-constant - radius
;   Excitations are done only at the center of the waveguide

(define-param DPML 1.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 1.) ; padding from wall

(define-param RADIUS 0.24)
(define-param EPSILON (* 1.95 1.95))

(define lattice-constant 1 )  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2) 0)) )

(define-param NUM_CELL_1 4)
(define NUM_CELL_2 NUM_CELL_1 )
(define-param LEN 20)

(define-param NUM_CELLS_WVG 0) ; number of cells of rods which will be removed in the center for the waveguide; 0 means the single center rod is removed
(define-param fcen 0.3)
(define-param df 0.5)
(define-param nfreq 200)
(define-param time_after_turnoff 300)
(define-param excite-mode? false)
(define-param transmission? false)  ; Set to true to study the transmission spectrum


; (define-param GRID_SIZE_X 10) ; useful size needed
(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y 
    (* 2 (+ 
        (* NUM_CELL_2 (vector3-y basis2) )
        PAD DPML
        )
    )
)
; Put mounting at the beginning and at the end of the rods
(define-param GRID_SIZE_Z 
    (+ LEN (* 2 PAD DPML) )
)


; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
(define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_2 basis2)))
(define thicknessAirBlock 
    (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1)
        (* 2 RADIUS)
    )
)

(set! geometryList (append     
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis2 ; shift-vector
        (* -1 NUM_CELL_2) ; min-multiple
        NUM_CELL_2 ; max-multiple
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list 
        (make block 
            (center cAirBlock )
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        ; cut off parallelogram at lower bottom corner
        (make block 
            (center (vector3* -1 cAirBlock ))
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
     
    )
    )
)


(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 )
      ; Large waveguide radius
      (radius (- lattice-constant RADIUS))
      (height infinity)
      (material air)
    )
  )
  )
)

; --------------------------------------------------------------------
; --------------------------------------------------------------------

(set! geometry geometryList )
; (set! epsilon-input-file "mounted_2D_PBG_wvg-out/eps-000000.00.h5")
; (set! epsilon-input-file "excite-mounted_2D_wvg-f-0.292/eps-000000.00.h5" )
; (set! eps-averaging? false)
(set-param! resolution 10)
(set! pml-layers (list (make pml (thickness DPML))))

(define dir-name (string-append "excite_unmounted_wvg-f-" (number->string fcen) "-df-" (number->string df)  ))

(if (not (= DPML 1.0))
  (set! dir-name (string-append dir-name "-pml-" (number->string DPML)  ) )
)
(if (not (= RADIUS 0.2))
  (set! dir-name (string-append dir-name "-r-" (number->string RADIUS)  ) )
)
(if (not (= NUM_CELL_1 4))
  (set! dir-name (string-append dir-name "-cells-" (number->string NUM_CELL_1)  ) )
)

(set! dir-name (string-append dir-name "-res-" (number->string resolution) ))
(if transmission? 
  (set! dir-name (string-append dir-name "-transmission"))
)

(use-output-directory dir-name)

(define src-pos-z (* 0.1 LEN) ) ; only for non-transmission simulations
(define src-size (* 2 (- lattice-constant RADIUS) ) ) ; only for transmission 
(define input-flux)  ; only for transmission 
(define trans-flux)  ; only for transmission 
(if transmission? 
  (begin
    (set! sources (list  
      (make source
        (src (make gaussian-src (frequency fcen) (fwidth df)) )
        (component Ez)
        (center 0 0 (* -0.5 LEN) )
      )
    ))
    (set! trans-flux
      (add-flux fcen df nfreq 
        (make flux-region 
          (center 0 0 (* 0.5 LEN) ) 
          (size (* 2 src-size) (* 2 src-size) 0 )
        )
      )
    )
    (set! input-flux
      (add-flux fcen df nfreq 
        (make flux-region 
          (center 0 0 (* -0.45 LEN) ) 
          (size (* 2 src-size) (* 2 src-size) 0 )
        )
      )
    )    
  )
  ; no transmission study
  (begin
    (set! sources (list
                   (make source
                     (src (make gaussian-src
                       (frequency fcen)
                       (fwidth df)
                       ))
                     (component Ez)
                     (center 0 0 src-pos-z)
                     )))  
  )
)


(if transmission?                 
  (begin
  (run-sources+ 
    (stop-when-fields-decayed
       50 Ez
       (vector3 0 0 (- (* 0.5 GRID_SIZE_Z) DPML PAD ) )
       1e-3
    )
    ; (at-beginning output-epsilon)
    ; (during-sources
      ; (in-volume  (volume (center 0 0 0) (size 0 0 GRID_SIZE_Z) )
      ; (to-appended "ez-slice" (at-every 0.21 output-efield-z)))
    ; )
  )
  (display-fluxes input-flux trans-flux)
  )
  (begin
  (run-sources+ time_after_turnoff
    (at-beginning output-epsilon)
    (after-sources (harminv Ez (vector3  0 0 (- src-pos-z) ) fcen df))
    )

  (if excite-mode?
    (run-until (/ 1 fcen)
      (to-appended "ez" (at-every (/ 1 fcen 20) output-efield-z) ) 
      ; (at-every  output-efield-z)
    )
  )
  )
)
; (run-until 1 ( at-beginning output-epsilon))




