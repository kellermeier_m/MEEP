# MEEP Simulations
----

This is a repo for the control scripts of MEEP simulations of a PBG woodpile
structure made out of glass. It is intended to be used for Laser Accelaration of
electrons with a THz Laser.

More information can be found on the (official webpage)[http://ab-initio.mit.edu/wiki/index.php/Meep]

Version overview
---
- The tag v0.5 is the point in history at which the final thesis was handed in
- In v0.6, the repo is cleaned up a lot, using only single files  for simulations instead of directories. Output files are clearly separeted from the simulation scripts

General Comments
---
- Control files: Those files describe the structure, the initial field, what to output and what to analyse after the simulation if possible.


Directories
----
* woodpile-python: Tried to define the structure via the python interface of MEEP.
Unfortunately, it wasn't successful due to the complicated 'stack' setup of a geometry via the python interface
* woodpile-scheme: woodpile stack mirrored at horizontal plane without waveguide hole
* woodpile-stack: the same as in 'woodpile-scheme' but additionally with waveguide
* Tutorial: Examples taken from the MEEP Tutorial for Scheme and for python
* Training: Modification of the MEEP example of a holey waveguide with goal of
minimizing phase velocity. Function defined and optimization defined.
* Examples: All examples provided by the MEEP webpage
* 2D-hexagonal-PBG-wvg: This folder examines two different geometries
  - 2D_PBG_waveguide: A finite, triangular lattice with hexagonal outer shape. At the lattice points the cylinders are placed and the center rod is removed to get a cavity like geometry. This geometry is treated as being two-dimensional in the simulations. All fields have wave vectors in the lattice plane. No out-of-plane simluations were done.
    In the directories called *2D_PBG_wvg-out-f-A.BCD* you can find the simulations with a fixed frequency
  - excite-mounted_2D_wvg: This geometry includes the periodic mounting. More than just a single mounting is used. For details look at the resulting epsilon distribition or the control file.
    Two frequencies resulted in nice confined modes. They are given in the directories *excite-mounted_2D_evg-out-f-A.BCD*. The simulation without given frequency was perfomed with a broad pulse as written in the conrol file 
* 2D-mounted-with-1D-Bands: The structure is treated infinitely extended in longitudinal direction. In the plane of the PBG lattice, the structure is finite. The 
* Mounted-2D-wvg-Transmission:
  NOT DONE YET 

Files
---




Structures
---
### The woodpile structure
The woodpile structrure is made of equal rectangular bars which are aligned parallel in a periodic manner in layers. The second layer is rotated w.r.t. the first one by 90 degree, while the third and the fourth are shifted by a half period with respect to the first layer and the second layer, respectively.

### The two-dimensional triangular PBG structure with mounting
Parallel cylinders of equal size are arranged in a triangluar lattice with a certain periodicity. To fix them they are mounted along the cylinder axis in regular distance. 


