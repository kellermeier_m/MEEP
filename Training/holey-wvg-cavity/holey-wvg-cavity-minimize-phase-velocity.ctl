; Meep Tutorial: TE transmission and reflection through a cavity
; formed by a periodic sequence of holes in a dielectric waveguide,
; with a defect formed by a larger spacing between one pair of holes.

; This structure is based on one analyzed in:
;    S. Fan, J. N. Winn, A. Devenyi, J. C. Chen, R. D. Meade, and
;    J. D. Joannopoulos, "Guided and defect modes in periodic dielectric
;    waveguides," J. Opt. Soc. Am. B, 12 (7), 1267-1272 (1995).

; Some parameters to describe the geometry:
(define-param eps 13) ; dielectric constant of waveguide
(define-param w 1.2) ; width of waveguide
(define-param r 0.36) ; radius of holes
(define-param d 1.4) ; defect spacing (ordinary spacing = 1)
(define-param N 3) ; number of holes on either side of defect

; The cell dimensions
(define-param sy 6) ; size of cell in y direction (perpendicular to wvg.)
(define-param pad 2) ; padding between last hole and PML edge
(define-param dpml 1) ; PML thickness

(define sx (+ (* 2 (+ pad dpml N)) d -1)) ; size of cell in x direction
(set! geometry-lattice (make lattice (size sx sy no-size)))

(set! geometry
      (append ; combine lists of objects:
       (list (make block (center 0 0) (size infinity w infinity)
		   (material (make dielectric (epsilon eps)))))
       (geometric-object-duplicates (vector3 1 0) 0 (- N 1)
	(make cylinder (center (/ d 2) 0) (radius r) (height infinity)
	      (material air)))
       (geometric-object-duplicates (vector3 -1 0) 0 (- N 1)
	(make cylinder (center (/ d -2) 0) (radius r) (height infinity)
	      (material air)))))

(set! pml-layers (list (make pml (thickness dpml))))
(set-param! resolution 20)

(define-param fcen 0.25) ; pulse center frequency                            
(define-param df 1.2)  ; pulse width (in frequency) 

(define-param nfreq 500) ; number of frequencies at which to compute flux


(set! sources (list
	       (make source
		 (src (make gaussian-src (frequency fcen) (fwidth df)))
		 (component Ez) (center 0.1234 0))))

(set! symmetries (list (make mirror-sym (direction Y) (phase +1))))

(define-param kx 0.5) ; if true, do run at specified kx and get fields
(define-param k-interp 19) ; # k-points to interpolate, otherwise

(define (print-freq-result)
    (print "Frequencies:   " (map harminv-freq-re harminv-results) "\n\n" )
    )

; (define (real-freqs-out)
    ; (map harminv-freq-re harminv-results)
; )

; (define (output-example)
    ; (with-output-to-file "b.txt" (lambda () (print "Hello World!") ) )
    ; )

; (define (my-weird-output) 
    ; (output-field-function "weird-function" (list) real-freqs-out)  
    ; )

(define (output-freqs)
    (with-output-to-file  (string-append (get-filename-prefix) "-freqs.out" )
        (lambda ()
            (print "Frequencies:   " (map harminv-freq-re harminv-results) "\n\n" )
        )
    )
)

(define (print-phase-velocity)
    (print "Phase velocity: " (phase-velocity) "\n\n")
)

(define (phase-velocity)
    (/ (car (map harminv-freq-re harminv-results) )  (vector-ref k-point 2) )    
)
   
(define (run-for-phase-velocity k)
    (set! k-point (vector3 0 0 k))
    (run-sources+ 300 
           (after-sources (harminv Ez (vector3 0.1234 0) fcen df 7) )
           (at-end print-phase-velocity)
           )
    (/ (car (map harminv-freq-re harminv-results) )  k   )    
)   

(run-for-phase-velocity 0.3)


; (if kx
    ; (begin
      ; (set! k-point (vector3 0 0 kx))
      ; (run-sources+ 300 
           ; (at-beginning output-example )
           ; (at-beginning output-epsilon)
           ; (at-beginning (lambda () (print "Hello World!") ) )
           ; (after-sources (harminv Ez (vector3 0.1234 0) fcen df 7) )
           ; (at-end print-freq-result) 
           ; (at-end output-freqs )
           ; (at-end print-phase-velocity)
           ; (at-end (output-field-function "weird-function" (list () ) real-freqs-out) )
        ; )
      ; (run-until (/ 1 fcen) (at-every (/ 1 fcen 20) output-efield-z)))
    ; (run-k-points 300 (interpolate k-interp (list (vector3 0 0 0) (vector3 0 0 0.5)))))

    

; (set! sources (list
     ; (make source
       ; (src (make gaussian-src (frequency fcen) (fwidth df)))
       ; (component Hz) (center 0 0))))

; (set! symmetries
; (list (make mirror-sym (direction Y) (phase -1))
  ; (make mirror-sym (direction X) (phase -1))))

; (run-sources+ 400
    ; (at-beginning output-epsilon)
    ; (after-sources (harminv Hz (vector3 0) fcen df)))
    
; (run-until (/ 1 fcen) (at-every (/ 1 fcen 20) output-hfield-z))      


