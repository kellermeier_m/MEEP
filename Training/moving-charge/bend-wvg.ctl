; From the Meep tutorial: transmission around a 90-degree waveguide
; bend in 2d.

(define-param sx 16) ; size of cell in X direction
(define-param sy 16) ; size of cell in Y direction
(set! geometry-lattice (make lattice (size sx sy no-size)))

(define-param pad 4) ; padding distance between waveguide and cell edge
(define-param w 1) ; width of waveguide

(define wvg-ycen (* -0.5 (- sy w (* 2 pad)))) ; y center of horiz. wvg
(define wvg-xcen (* 0.5 (- sx w (* 2 pad)))) ; x center of vert. wvg

(define-param no-bend? false) ; if true, have straight waveguide, not bend

(set! geometry
	  (list
	   (make block
	     (center (* -0.5 pad) wvg-ycen)
	     (size (- sx pad) w infinity)
	     (material (make dielectric (epsilon 12))))
	   (make block
	     (center wvg-xcen (* 0.5 pad))
	     (size w (- sy pad) infinity)
	     (material (make dielectric (epsilon 12))))))

(set-param! k-point (vector3 1 0))
(define-param fcen 0.15) ; pulse center frequency
(define-param df 0.1)  ; pulse width (in frequency)

(set! sources (list
	       (make source
		 (src (make gaussian-src (frequency fcen) (fwidth df)))
		 (component Ez)
		 (center (+ 1 (* -0.5 sx)) wvg-ycen)
		 (size 0 w))))
(set! pml-layers (list (make pml (thickness 1.0))))
(set-param! resolution 10)
(run-until 200
           (at-beginning output-epsilon)
           (to-appended "ez" (at-every 0.6 output-efield-z)))
