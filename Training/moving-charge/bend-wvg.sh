#!/bin/bash

meep-mpi bend-wvg.ctl
h5topng -t 0:329 -R -Zc dkbluered -a yarg -A bend-wvg-eps-000000.00.h5 bend-wvg-ez.h5
convert bend-wvg-ez.t*.png ez.gif
