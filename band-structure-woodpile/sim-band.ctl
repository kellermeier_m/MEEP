(define-param PAD 1.) ; padding from wall

; Geometry parameters
; Distance between rods in x resp. y direction
(define-param DISTANCE 1.)  ; corresponds to 'a'  in Cowan, B.
(define-param VERTICAL_PERIOD (* DISTANCE (sqrt 2)) ); corresponds to 'c'  in Cowan, B.
; Rod dimensions
(define-param RODWIDTH 0.25)  ; corresponds to 'w'  in Cowan, B.
(define-param RODHEIGHT (* VERTICAL_PERIOD 0.25 ) ) ; corresponds to 'c'/4  in Cowan, B.,
      ; c= a * sqrt(2) gives a cubic lattice
; TODO: Two different rod lengths are neccessary in FUTURE


(define-param GRID_SIZE_X DISTANCE) ;
(define-param GRID_SIZE_Y DISTANCE) ;
(define-param GRID_SIZE_Z (* 4 RODHEIGHT)  )
; (define-param GRID_SIZE_Z (+ (* 4 RODHEIGHT)  6 )) ; increased size due to added pml layer
; (define-param DPML PAD)  ; thickness of the pml layer

; Source parameters
(define-param fcen 0.35) ; pulse center frequency
(define-param df 1.8)  ; pulse width (in frequency)

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material (make dielectric (epsilon 1)) )

;geometry
(define geometryList (list  ) )
; material
(define Silicon (make dielectric (epsilon 12.1)))
(define Glass (make dielectric(epsilon 3.7)))     ; at THz frequency
; first layer
; REMARK: The z coordinate of the center is negative because the negative half cube is the symmetry
;           defining half cube. If the rods are placed in the upper half cube they will be erased
;           by exploiting the symmetry

;first layer
(set! geometryList (append geometryList
        (list
          (make block
              (center 0 0 (* -1.5 RODHEIGHT ) )
              (size RODWIDTH DISTANCE RODHEIGHT)
              (material Glass)
          )
          (make block
              (center 0 0 (* -0.5 RODHEIGHT ) )
              (size DISTANCE RODWIDTH RODHEIGHT)
              (material Glass)
          )
          ;(geometric-object-duplicates (vector3 DISTANCE 0 0) 0  1
            (make block
                (center (* -0.5 DISTANCE)  0 (* 0.5 RODHEIGHT ) )
                (size RODWIDTH DISTANCE RODHEIGHT)
                (material Glass)
            )
            (make block
                (center (* 0.5 DISTANCE)  0 (* 0.5 RODHEIGHT ) )
                (size RODWIDTH DISTANCE RODHEIGHT)
                (material Glass)
            )
          ;)
          ;(geometric-object-duplicates (vector3 0 DISTANCE 0) 0  1
            (make block
                (center 0 (* -0.5 DISTANCE)  (* 1.5 RODHEIGHT ) )
                (size DISTANCE RODWIDTH RODHEIGHT)
                (material Glass)
            )
            (make block
                (center 0 (* 0.5 DISTANCE)  (* 1.5 RODHEIGHT ) )
                (size DISTANCE RODWIDTH RODHEIGHT)
                (material Glass)
            )
          ;)

        )
    )
)

; Define the waveguide hole in transverse plane
(set! geometry geometryList )

; TODO: Actually not correct!
; (set! pml-layers (list (make pml (direction Z) (thickness 0.2))))

(set! resolution 25)


; TODO: Define source
(set! sources (list
               (make source
                 (src (make gaussian-src (frequency fcen) (fwidth df)))
                 ; (component Hx)
                 (component Ex)
                 ; (component Ez)
                 (center
                   0.1234   ; try random
                   0.1930   ; try random
                   0.275
                  )
               )
              )
)

; Simulation settings
(define-param kx false) ; if true, do run at specified kx and get fields
(define-param k-interp 10) ; # k-points to interpolate, otherwise

(define GAMMA (vector3 0 0 0))
(define X (vector3 (/ 0.5 DISTANCE) 0 0) )
(define Y (vector3 0 (/ 0.5 DISTANCE) 0) )
(define Z (vector3 0 0 (/ 0.5 VERTICAL_PERIOD) ) )
(define M (vector3 (/ 0.5 DISTANCE) (/ 0.5 DISTANCE) 0) )
(define R (vector3 (/ 0.5 DISTANCE) (/ 0.5 DISTANCE) (/ 0.5 VERTICAL_PERIOD) ) )
(define T (vector3 0 (/ 0.5 DISTANCE) (/ 0.5 VERTICAL_PERIOD) ) )


(define k-points (list  ; REMARK! Here given as a vector in Cartesian coordinates, NOT Reciprocal basis
    GAMMA X M R X GAMMA Z R T Z GAMMA
  )
)

(use-output-directory)

(if kx
    (begin
      (set! k-point (vector3 kx))
      (run-sources+
       300 (at-beginning output-epsilon)
       (after-sources (harminv Ex (vector3 0.1234 0) fcen df)))
      (run-until (/ 1 fcen) (at-every (/ 1 fcen 20) output-hfield-z) )
      )
    (run-k-points 300 (interpolate k-interp  k-points)))

    
;;;; WORKS -> Gives band structure via
; meep-mpi sim-band.ctl | tee sim-band.out
;  grep freqs: sim-band.out > fre.dat


    ;(run-until 2
    ;  (at-beginning output-epsilon)
    ;  ;           (at-end output-efield-z)
    ;  )
