; 22.6.2017, Max Kellermeier
; Steps:
; Define Geometry, parameters:
; - thickness of the taper THICK
; - outer radius 
; - inner radius 
; - length of the horn l
; - cut angle
; Position of the source 
; 
(define-param DPML 2)
(define-param PAD 1)

; same inner radius as 2D PBG structure, keep that in mind to adjust
; a - rodradius
(define-param RODRADIUS 0.24)
(define lattice_const 1) ; lattice constant a is again the reference scale
;geometry
(define-param LEN 12)  ; length of the wvg
(define-param inner_radius 2)
(define-param outer_radius 2.75)
(define-param metal_radius 3.25)
(define-param cut_angle 30)
(define-param radiation_angle 30)
(define-param srcDistance 8)
; source
(define fcen 0.16)
(define df 0.1)

; computational cell
(define grid-size-x (* 2 (+ LEN DPML PAD) ) )
(define grid-size-y (* 2 (+ LEN DPML PAD) ) )
(define grid-size-z LEN )
(define fused_silica (make dielectric (epsilon 3.8)))
; (define outer_material (make dielectric (epsilon 8 )  ) )
(define outer_material metal)


(define (degToRad phi)  (* (/ phi 360 ) 2 pi)  )
(define cut_angle_rad (degToRad cut_angle) )
(define radiation_angle_rad (degToRad radiation_angle) )
(define cuttedCrossSectionDiameter 
  (/ (* 2 metal_radius ) (cos  cut_angle_rad)  ) 
)   ; hypothenuse of the cutted triangle H = 2R/cos(alpha)
(define cutoffDepth  (* cuttedCrossSectionDiameter (sin cut_angle_rad) )  )

(define geometryList (list 
    (make cylinder
      (center (/ LEN 2) 0 0)
      (material outer_material)
      (radius metal_radius)
      (height LEN)
      (axis (vector3 1 0 0))
    )
    (make cylinder
      (center (/ LEN 2) 0 0)
      (material fused_silica)
      (radius outer_radius)
      (height LEN)
      (axis (vector3 1 0 0))
    )
    (make cylinder
      (center (/ LEN 2) 0 0)
      (material air)
      (radius inner_radius)
      (height LEN)
      (axis (vector3 1 0 0))
    )
    (make block 
      (size cutoffDepth cuttedCrossSectionDiameter (* 2 metal_radius) )
      (center 0 0 0)
      (e2 (sin (degToRad cut_angle) ) (cos (degToRad cut_angle)) )
      (material air)
    )
  )
) 

; src position
(define entranceCenter (vector3 (/ cutoffDepth 2) 0 0 ) )
(define srcDirection (vector3 (- (cos cut_angle_rad)) (sin cut_angle_rad) ))
(define srcPosition
 (vector3+ entranceCenter (vector3* srcDistance srcDirection) )
)
(define src_temporal (make continuous-src (frequency fcen) (width 0.5) ) )
      (make gaussian-src (frequency fcen) (fwidth df) )
(define srcList (list  
    (make source
     (component Ex)
     (src src_temporal)
     (center srcPosition)
     (amplitude (sin radiation_angle_rad) )
    )
    (make source
     (component Ey)
     (src src_temporal)
     (center srcPosition)
     (amplitude (cos radiation_angle_rad) )
    )
  )
)


(set-param! resolution 15)
(set! geometry-lattice (make lattice (size grid-size-x grid-size-y grid-size-z)))
(set! pml-layers (list (make pml (thickness DPML))))
(set! geometry geometryList)
(set! sources srcList) 
 
; Set output name ----
(define dir-name "cut-coupler")

; (if (not  (= NUM_CELL_LONGITUDINAL 4) )
  ; (set! dir-name (string-append dir-name "-lcells-" (number->string NUM_CELL_LONGITUDINAL)  ) )  
; )

(set! dir-name (string-append dir-name "-res-" (number->string resolution) ))
; (set! dir-name (string-append dir-name "-geom-" (number->string geometry-num) ))
(use-output-directory dir-name)
;---------------------

(run-until 600 
    (at-beginning output-epsilon)
    (to-appended "ex" (at-every (/ 1 fcen 10) output-efield-x))
    (to-appended "ey" (at-every (/ 1 fcen 10) output-efield-y))
)
