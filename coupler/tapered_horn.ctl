; 22.6.2017, Max Kellermeier
; Steps:
; Define Geometry, parameters:
; - thickness of the taper THICK
; - outer radius at entrance b
; - inner radius at entrance a
; - outer radius of wvg. R
; - inner radius of wvg. r
; - length of the horn l
; include cylindrical symmetry for coordinate system
; Define TEM source at entrance

(define-param DPML 1)
(define-param PAD 0.5)

; same inner radius as 2D PBG structure, keep that in mind to adjust
; a - rodradius
(define-param RODRADIUS 0.24)
(define lattice-const 1) ; lattice constant a is again the reference scale

(define-param LEN 10)  ; length of the horn
(define-param entrance_inner 3) ;
(define-param THICK 0.2)    ;
(define-param METALLAYER 0.2) ;
(define inner_radius (- lattice-const RODRADIUS))
(define outer_radius (+ inner_radius THICK) )
(define entrance_outer (+ entrance_inner THICK) )
(define metal_outer (+ entrance_outer METALLAYER) )

(define-param EPSILON (* 1.95 1.95))
(define Glass (make dielectric (epsilon EPSILON)))

(define  grid-size-z
  (* 2 (+ LEN PAD DPML) )
)
(define grid-size-r
  (+ metal_outer PAD DPML)
)
; cylindrical grid with no dependence in phi (anyways not supported)
(set! dimensions CYLINDRICAL)
(set! geometry-lattice (make lattice (size grid-size-r no-size grid-size-z) ) )

(set-param! m 0)

;----------------- Define geometry
; Total length of the horn
; LEN only specifies the projection on to the z-axis
(define totlength (sqrt (+
    (expt LEN 2) (expt (- entrance_inner inner_radius )  2)   
   ) 
) )
; the length of the waveguide part is extended to the pml
(define line_length
  (+ LEN PAD DPML)
)

(define dummy-mat (make dielectric (index 2.5)))
(define dummy-mat metal)


(define geometryList (list
                (make block 
                  (center (+ inner_radius (/ THICK 2)) 0 (/ line_length 2) ) 
                  (size THICK infinity line_length)
                  (material Glass) )
                (make block
                  (center (+ outer_radius (/ METALLAYER 2) ) 0 (/ line_length 2) )
                  (size METALLAYER infinity line_length)
                  (material dummy-mat)
                )
                (make block
                  ; radial center position is:
                  ; THICK/2 + (entrance_inner+inner_radius)/2
                  (center  (/ (+ entrance_inner  THICK inner_radius) 2) 0 (/ LEN -2)  )
                  ; (e3 (vector3 (+ entrance_inner (/ THICK 2) ) 0 (- LEN) ) )
                  (e3 (vector3
                    (- entrance_inner   inner_radius) 0 (- LEN )
                  ))
                  (size THICK infinity totlength)
                  (material Glass)
                )
                (make block
                  ; radial center position is:
                  ; THICK/2 + (entrance_inner+inner_radius)/2
                  (center  (/ (+ entrance_outer  METALLAYER outer_radius) 2) 0 (/ LEN -2)  )
                  ; (e3 (vector3 (+ entrance_inner (/ THICK 2) ) 0 (- LEN) ) )
                  (e3 (vector3
                    (- entrance_inner  inner_radius) 0 (- LEN )
                  ))
                  (size METALLAYER infinity totlength)
                  (material dummy-mat)
                )
  )
)

(set! geometry geometryList)
(set! pml-layers (list (make pml (thickness DPML))))
(set! resolution 16)

; DUMMY FIELD up to now
(define srcSize (* 2 entrance_inner 0.9) )
(set! sources (list
               (make source
                 (src (make continuous-src (frequency 0.15)))
                 (component Er)
                 (center 0 0 (- LEN) )
                 (size srcSize)
                ))
 )
 
 
(use-output-directory)

(run-until 100 
  ; (in-volume (volume (center 0) (size (* 2 grid-size-r)))
    (at-beginning output-epsilon)
  ; )
)
(run-until (/ 0.15)
  (to-appended "ez" (at-every (/ 1 0.15 20) output-efield-z))
  (to-appended "er" (at-every (/ 1 0.15 20) output-efield-r))
)


