; 05.06.2017, Max Kellermeier
; Steps:
; 1. Define geometry of interest
; 2. Define geometry of reference for transmission. Could be no structure at all, but probably it's
; 3. Boolean for switching between the geometries
; 4. Define area where the power is measured
; 5. Set Source

(define-param DPML 1.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 4.) ; padding from wall

(define-param RADIUS 0.24)    ; Changed from 0.2 to 0.24 as ideal Radius
(define-param EPSILON (* 1.95 1.95))
(define-param MOUNTING_THICKNESS_REL 0.1 ) ; longitudinal thickness relative to longitudinal spatial period 'longitudinal-constant'
(define-param K_Z 0.0)  ; Out-of-plane component of the
(define-param large-src? true)  ; Either the source has the size of the waveguide radius or the distance of the rods
(define-param fcen 0.47)
(define-param df 0.1)
(define-param transmission? false)  ; Set to true to study the transmission spectrum
; (define-param reference-geom? false) ; Set to true to study the reference geometry ; NOT USED ANYMORE
(define-param geometry-num 0) ; 0 means Geometry of interest, 1 means filled waveguide (no pbg setup), 2 means free space
(define-param rel-long-constant 5) ; longitudinal constant relative to the reference scale a

(define lattice-constant 1 )  ; reference scale, a
(define longitudinal-constant (* rel-long-constant lattice-constant) ) ; longitudinal spatial period c, in terms of a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2) 0)) )
(define basis3 (vector3 0 0 longitudinal-constant ))

(define-param NUM_CELL_1 4)
(define-param NUM_CELL_2 NUM_CELL_1 )
(define-param NUM_CELL_LONGITUDINAL 4)
(define-param NUM_CELLS_WVG 0) ; number of cells of rods which will be removed in the center for the waveguide; 0 means the single center rod is removed
(define-param MOUNTING_RADIUS (* (+ NUM_CELL_1 1) lattice-constant ) ) ; radius of mounting plate is one cell larger than the rod lattice to make sure all rods are completely covered

(define length-z (* 2 
        (* (+ NUM_CELL_LONGITUDINAL 1) longitudinal-constant)  ; (NUM_CELL_LONGITUDINAL + 1) * longitudinal-constant * 2
))

; (define-param GRID_SIZE_X 10) ; useful size needed
(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y 
    (* 2 (+ 
        (* NUM_CELL_2 (vector3-y basis2) )
        PAD DPML
        )
    )
)
; Put mounting at the beginning and at the end of the rods
(define-param GRID_SIZE_Z 
    (+ (* 2 PAD DPML ) length-z)
)
; (NUM_CELL_LONGITUDINAL * longitudinal-constant )

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height length-z) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
(define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_2 basis2)))
(define thicknessAirBlock 
    (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1)
        (* 2 RADIUS)
    )
)

(set! geometryList (append     
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis2 ; shift-vector
        (* -1 NUM_CELL_2) ; min-multiple
        NUM_CELL_2 ; max-multiple
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list 
        (make block 
            (center cAirBlock )
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        ; cut off parallelogram at lower bottom corner
        (make block 
            (center (vector3* -1 cAirBlock ))
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
     
    )
    )
)

; Periodic mounting 
(set! geometryList ( append geometryList
    (geometric-object-duplicates
      basis3 ; direction for the duplicates
      (* -1 NUM_CELL_LONGITUDINAL)  ; minimum of the multiples of the direction
      NUM_CELL_LONGITUDINAL ; maximum of the multiples of the direction
      (make cylinder  ; geometric object
        (center 0 0 0)
        (radius MOUNTING_RADIUS)
        (height (* MOUNTING_THICKNESS_REL longitudinal-constant ) )
        (material Glass)
      )
    )
  )  
)

(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 0)
      ; Large waveguide radius
      (radius (- lattice-constant RADIUS))
      (height infinity)
      (material air)
    )
  )
  )
)

(define refRadius (+ lattice-constant (- RADIUS) (* (vector3-x basis1) NUM_CELLS_WVG) ) )
; large radius for reference geometry
(set! refRadius (- lattice-constant RADIUS))

(define refGeometry ( list 
    ; large glass cylinder
    (make cylinder (center 0 0 0) (radius MOUNTING_RADIUS) (height length-z) (material Glass) )
    ; remove air cylinder
    ; Waveguide hole
    (make cylinder (center 0 0 0) (radius  refRadius) (height infinity) (material air) )
  )
)

; free space
(define refGeometry2 (list))

; --------------------------------------------------------------------
; --------------------------------------------------------------------
; --------------------------------------------------------------------

(cond 
  ((= geometry-num 0) (set! geometry geometryList))
  ((= geometry-num 1) (set! geometry refGeometry))
  ((= geometry-num 2) (set! geometry refGeometry2))
)
      

; (set! epsilon-input-file "../2D-hexagonal-PBG-wvg/mounted_2D_PBG_wvg-out/eps-000000.00.h5")
(set-param! resolution 20)
; (set! resolution 5)
(set! pml-layers (list (make pml (thickness DPML))))

(define dir-name)
(if transmission?
  (set! dir-name "spectrum-"  )
  (set! dir-name "transmission-"  )
)

(if large-src? 
  (set! dir-name (string-append dir-name "large-src-f-" (number->string fcen)  ) )
  (set! dir-name (string-append dir-name "small-src-f-" (number->string fcen)  ) )
)
(if (not (= DPML 1.0))
  (set! dir-name (string-append dir-name "-pml-" (number->string DPML)  ) )
)
(if (not (= RADIUS 0.2))
  (set! dir-name (string-append dir-name "-r-" (number->string RADIUS)  ) )
)
(if (not (= NUM_CELL_1 4))
  (set! dir-name (string-append dir-name "-cells-" (number->string NUM_CELL_1)  ) )
)
(if (not (= rel-long-constant 5) ) 
  (set! dir-name (string-append dir-name "-lconst-" (number->string rel-long-constant)  ) )
)
(if (not  (= NUM_CELL_LONGITUDINAL 4) )
  (set! dir-name (string-append dir-name "-lcells-" (number->string NUM_CELL_LONGITUDINAL)  ) )  
)

(set! dir-name (string-append dir-name "-res-" (number->string resolution) ))
(set! dir-name (string-append dir-name "-geom-" (number->string geometry-num) ))
(use-output-directory dir-name)

(define src-size 0)
(if large-src? 
  (set! src-size (* 2 (- lattice-constant RADIUS) ))
  (set! src-size (* 2 RADIUS)) 
)

(if transmission?  ; for the moment only run the continuous source
  (set! sources (list
                 (make source
                   (src (make gaussian-src (frequency fcen) (fwidth df)  )              
                    )
                   (component Ez)
                   ; (center 0 0 ( * -1 NUM_CELL_LONGITUDINAL longitudinal-constant ))
                   (center 0 0 
                      (+ DPML PAD (* -0.5 GRID_SIZE_Z) )
                   )
                   ; check different sizes
                   ; (size RADIUS RADIUS 0 )
                   (size src-size src-size 0)
               )
  ))

  (set! sources (list
                 (make source
                   (src (make continuous-src
                     (frequency fcen)
                     ))
                   (component Ez)
                   (width 7)
                   ; (center 0 0 ( * -1 NUM_CELL_LONGITUDINAL longitudinal-constant ))
                   (center 0 0 
                      (+ DPML PAD (* -0.5 GRID_SIZE_Z) )
                   )
                   ; check different sizes
                   ; (size RADIUS RADIUS 0 )
                   (size src-size src-size 0)
               )
  ))
)
              
; ; flux layer
(define-param nfreq 100) ; number of frequencies at which to compute flux
(define trans   ; transmitted flux                                                
      (add-flux fcen df nfreq
          (make flux-region
           ; TODO
           (center 0 0 (- (* 0.5 GRID_SIZE_Z) DPML PAD )  ) 
           (size (* 2 src-size) (* 2 src-size) 0)
          )
       )
)
; ---- TODO: Exploring Symmetry
; 210 time units corresponds to 100 periods since T=2.1
; run 
(if (not transmission?)
  (run-until 210
    (at-beginning output-epsilon)
    (to-appended "ez" (at-every 0.21 output-efield-z))
  )
)

                  
(if transmission?
(begin
  (run-sources+ 
    (stop-when-fields-decayed
       50 Ez
       (vector3 0 0 (- (* 0.5 GRID_SIZE_Z) DPML PAD ) )
       1e-3
    )
    ; (at-beginning output-epsilon)
    ; (during-sources
      ; (in-volume  (volume (center 0 0 0) (size 0 0 GRID_SIZE_Z) )
      ; (to-appended "ez-slice" (at-every 0.21 output-efield-z)))
    ; )
  )
  (display-fluxes trans) ; print out the flux spectrum
)
)
