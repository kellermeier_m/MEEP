; pseudo Code for running meep until the phase velocity matches BETA

( define BETA 0.25)
( define TOL 0.05)

(define wvc 1)
(define freq 0.5)


; (set-param! k-point (vector3 0.4 0))
; (run-sources+ 300 (after-sources (harminv Hz (vector3 0.1234) fcen df)))

; TODO:  geometry defined

(define (phase-velocity-diff )
    ; TODO
    (retrieve (harminv-freq-re result) )
)



; Something like that will be needed
(set! k-point (vector3 kx))
      (run-sources+ 
       300 (at-beginning output-epsilon)
       (after-sources 
            (harminv Hz (vector3 0.1234 0) fcen df 7)
            
            )
       )
      (run-until (/ 1 fcen) (at-every (/ 1 fcen 20) output-hfield-z))


; Not Needed since a minimize func can be used
; (while (cond)
    ; (print )
    ; )

