(define-param DPML 2.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 1.) ; padding from wall

(define-param EPSILON (* 1.95 1.95))

(define lattice-constant 1 )  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2))) )

(define-param NUM_CELL_1 4)
(define-param NUM_CELL_2 NUM_CELL_1 )
(define-param NUM_CELLS_WVG 0)

(define-param fcen 0.4)
(define-param df 0.6)

(define-param GRID_SIZE_X
    (* 2 (+
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y
    (* 2 (+
        (* NUM_CELL_2 (vector3-y basis2) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Z no-size )


; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material air )


(set! resolution 16)
(set! pml-layers (list (make pml (thickness DPML))))

(define dir-name (string-append "2D_optimize-Q-f-" (number->string fcen) "-df-" (number->string df)  ))

(if (not (= DPML 1.0))
  (set! dir-name (string-append dir-name "-pml-" (number->string DPML)  ) )
)
(if (not (= RADIUS 0.2))
  (set! dir-name (string-append dir-name "-r-" (number->string RADIUS)  ) )
)
(if (not (= NUM_CELL_1 4))
  (set! dir-name (string-append dir-name "-cells-" (number->string NUM_CELL_1)  ) )
)
(if (not  (= resolution 16) )
  (set! dir-name (string-append dir-name "-res-" (number->string resolution) ) )
)

(use-output-directory dir-name)



(define (set-geometry r) 
  (let* 
    (  
    (cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_2 basis2)) ) 
    (thicknessAirBlock (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1) (* 2 r) )
    )
    (geometryList
      (geometric-objects-duplicates
        basis2 ; shift-vector
        (* -1 NUM_CELL_2) ; min-multiple
        NUM_CELL_2 ; max-multiple
        (geometric-object-duplicates
          basis1 ; shift-vector
          (* -1 NUM_CELL_1) ; min-multiple
          NUM_CELL_1 ;max-multiple
          (make cylinder (center 0 0 0) (radius r) (height infinity) (material Glass))
        )
      )
    )
    )
    (set! geometry (append geometryList
      (list
          (make block
              (center cAirBlock )
              (size thicknessAirBlock infinity)
              (e1 1.5 (/ (sqrt 3) 2) )
              (e2 -0.5 (/ (sqrt 3) 2) )
              (material air)
          )
          (make block
              (center (vector3* -1 cAirBlock ))
              (size thicknessAirBlock infinity)
              (e1 1.5 (/ (sqrt 3) 2) )
              (e2 -0.5 (/ (sqrt 3) 2) )
              (material air)
          )
          ; Waveguide hole
          (make cylinder
              (center 0 0 )
              (radius (+ r (* (vector3-x basis1) NUM_CELLS_WVG) ) )
              (height infinity)
              (material air)
          )
      )
      )
    )
  )
)



  ; (set! sources (list
    ; (make source
      ; (src (make gaussian-src
        ; (frequency fcen)
        ; (fwidth df)
        ; )
      ; )
      ; (component Ez)
      ; (center 0 0 0)
      (size 1 1 )  
      ; )
    ; )
  ; )

(define (largest-Q-by-radius r) 
  (reset-meep)
  (set-geometry r)

  (set! sources (list
    (make source
      (src (make gaussian-src
        (frequency fcen)
        (fwidth df)
        )
      )
      (component Ez)
      (center 0 0 0)
      ; (size 1 1 )  
      )
    )
  )    
  (run-sources+ 300
   (at-beginning output-epsilon)
   (after-sources (harminv Ez (vector3 0 0) fcen df) )
  )  
  (let  ((Q-list (map harminv-Q harminv-results) ) )
    (if (null? Q-list) 
      0  
      (apply max  Q-list)
    )
  )
)

(define result (maximize largest-Q-by-radius 0.01 0.1 1) ) 
(print "Maximum of Q:" ( max-val result) " at r=" (max-arg result)  )

; (largest-Q)
;TODO: test 'set-geometry 0.4'





