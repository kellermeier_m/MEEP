; 05.06.2017, Max Kellermeier
; Steps:
; 1. Define geometry of interest
; 2. Define geometry of reference for transmission. Could be no structure at all, but probably it's
; 3. Boolean for switching between the geometries
; 4. Define area where the power is measured
; 5. Set Source

(define-param DPML 3.) ; Used as thickness of the Perfectly matched layer
(define-param PAD 2.) ; padding from wall

(define-param RADIUS 0.38)    
(define-param EPSILON (* 1.95 1.95))
(define-param DEFECT 0.97)
(define-param fcen 0.52)
(define-param df 0.1)
(define-param NUM_CELL_1 5)  ; CURRENTLY LIMITED TO ~ 20 (test 25 to see problem)
(define-param time-after-src 300) ; waiting time after sources are off before remaining field are analyzed


(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* 1 (vector3 1 0 0) ))
(define basis2 (vector3* 1 ( vector3 0.5 (/ (sqrt 3) 2) 0)) )

; Two options:
; 1. using periodic boundary condition
; 2. use magnetic material at boundary, mu=infty
(define use-periodic? true)  

(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y GRID_SIZE_X)

; ----------------------------------------------------------------------------
(set! geometry-lattice 
  (make lattice (size GRID_SIZE_X GRID_SIZE_Y no-size)))
(set! default-material air)

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
(define sum_basis (vector3+ basis1  basis2) )
(define cAirBlock 
  (vector3+ 
    (vector3* (* 0.75  NUM_CELL_1) sum_basis)  
    (vector3* RADIUS (unit-vector3 sum_basis) )
  ) 
)

(define thicknessAirBlock 
    (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1 ) 
        RADIUS
    )
)
(define heightAirBlock (+ NUM_CELL_1 2)) 


(set! geometryList (append     
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis2 ; shift-vector
        (* -1 NUM_CELL_1) ; min-multiple
        NUM_CELL_1 ; max-multiple
        geometryList
    )    
    (list
        (make block
            (center cAirBlock )
            (size (* 0.5 thicknessAirBlock) heightAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) ) ; sum of basis_1 and basis_2
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
       ; cut off parallelogram at lower bottom corner
        (make block
            (center (vector3* -1 cAirBlock ))
            (size (* 0.5 thicknessAirBlock) heightAirBlock infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )

    )
    
    
  )
)

(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 0)
      (radius DEFECT)
      (height infinity)
      (material air)
    )
  )
  )
)

(set! geometry geometryList)
; --------------------------------------------------------------------
; --------------------------------------------------------------------
; --------------------------------------------------------------------
      
; (set! epsilon-input-file "../2D-hexagonal-PBG-wvg/mounted_2D_PBG_wvg-out/eps-000000.00.h5")
(set-param! resolution 20)

(set! pml-layers (list 
  (make pml (direction Y) (thickness DPML))
  (make pml (direction X) (thickness DPML))
  ))

(define  dir-name "eigenmode-src-2D-"  )

(if (not (= DPML 3.0))
  (set! dir-name (string-append dir-name "-pml-" (number->string DPML)  ) )
)
(if (not (= RADIUS 0.38))
  (set! dir-name (string-append dir-name "-r-" (number->string RADIUS)  ) )
)

(set! dir-name (string-append dir-name "-cells-" (number->string NUM_CELL_1)  ) )
(set! dir-name (string-append dir-name "-t_after-" (number->string time-after-src)  ) )

(set! dir-name (string-append dir-name "-res-" (number->string resolution) ))
(use-output-directory dir-name)

; (define src-size DEFECT)
         
(set-param! k-point (vector3 0 0 0.52))          

; TODO----------
(set! sources (list
  (make eigenmode-source
      (src (make gaussian-src (frequency fcen) (fwidth df)))
      (component Ez)
      (size 5 (* 5 (/ (sqrt 3) 2))  )
      (center 0 0)
      (eig-kpoint k-point)
      (eig-match-freq? true)
;       (eig-parity ODD-Y))
  )
))



(run-sources+ time-after-src 
  (at-beginning output-epsilon)
  (after-sources (harminv Ez (vector3 0 0 0) fcen df))
)
(run-until (/ 1 fcen ) 
  (to-appended "ez-at-end" (at-every (/ 1 fcen 20) output-efield-z) )
)


