; Author: Max Kellermeier, max.kellermeier@hotmail.de
; Desc: a relativistic particle is sent through the infinitely extended 2D PBG structure. The particle wakefield excites resonant modes inside the waveguide. The structure consists of glass cylinders whose ends are ignored. Within the computational cell the cylinders appear to float in vacuum and are not attached to each other (no mounting).
; COMMAND: meep 
; Open Questions: 
;	what should be the size along the z-axis?
;	what size should the PML have?

(define-param DPML 2.5) ; Used as thickness of the Perfectly matched layer
(define-param PAD 1.5) ; padding from wall

(define-param EPSILON (* 1.95 1.95))
(define-param RADIUS 0.38)
(define-param DEFECT 0.97)
;? (define-param fcen 0.47)
;? (define-param df 0.1)

(define lattice-constant 1 )  ; reference scale, a
(define Glass (make dielectric (epsilon EPSILON)))
(define basis1 (vector3* lattice-constant (vector3 1 0 0) ))
(define basis2 (vector3* lattice-constant ( vector3 0.5 (/ (sqrt 3) 2) 0)) )
;? (define basis3 (vector3 0 0 longitudinal-constant ))
(define-param NUM_CELL_1 4)

; (define-param GRID_SIZE_X 10) ; useful size needed
(define-param GRID_SIZE_X 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-x basis1) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Y 
    (* 2 (+ 
        (* NUM_CELL_1 (vector3-y basis2) )
        PAD DPML
        )
    )
)
(define-param GRID_SIZE_Z 20 )
; ----------------------------------------------------------------------------
; Grid and Geometry
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
(set! default-material air )

(define geometryList
  (geometric-object-duplicates
  basis1 ; shift-vector
  (* -1 NUM_CELL_1) ; min-multiple
  NUM_CELL_1 ; max-multiple
  (make cylinder (center 0 0 0) (radius RADIUS) (height infinity) (material Glass) )
))

; remove part from parallelogram to make it hexagonal
; center 
(define cAirBlock (vector3+ (vector3* NUM_CELL_1 basis1) (vector3* NUM_CELL_1 basis2)))
(define thicknessAirBlock 
    (- (* (vector3-norm (vector3+ basis1 basis2)) NUM_CELL_1)
        (* 2 RADIUS)
    )
)

(set! geometryList (append     
    ; Construct parallelogram of lattice cells
    (geometric-objects-duplicates
        basis2 ; shift-vector
        (* -1 NUM_CELL_1) ; min-multiple
        NUM_CELL_1 ; max-multiple
        geometryList
    )
    ; cut off parallelogram at upper right corner
    (list 
        (make block 
            (center cAirBlock )
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
        ; cut off parallelogram at lower bottom corner
        (make block 
            (center (vector3* -1 cAirBlock ))
            (size thicknessAirBlock infinity infinity)
            (e1 1.5 (/ (sqrt 3) 2) )
            (e2 -0.5 (/ (sqrt 3) 2) )
            (material air)
        )
     
    )
    )
)

(set! geometryList ( append geometryList
; Waveguide hole
  (list
    (make cylinder
      (center 0 0 0)
      ; Large waveguide radius
      (radius DEFECT)
      (height infinity)
      (material air)
    )
  )
  )
)

; (set! epsilon-input-file "../2D-hexagonal-PBG-wvg/mounted_2D_PBG_wvg-out/eps-000000.00.h5")
; --------------------------------------------------------------------
; Particle properties
(define-param v 1)
(define-param endt (/ GRID_SIZE_Z v))

; --------------------------------------------------------------------    
(set-param! resolution 15)
(set! geometry geometryList)
(set! filename-prefix "particle-")
; (set! epsilon-input-file "second-eps-000000.00.h5")
; (set! eps-averaging? false)
; (set! filename-prefix "second2")

(set! pml-layers (list (make pml (thickness DPML))))

; TODO----------------------------------------------------
; (if large-src? 
  ; (set! dir-name (string-append dir-name "large-src-f-" (number->string fcen)  ) )
  ; (set! dir-name (string-append dir-name "small-src-f-" (number->string fcen)  ) )
; )
; (set! dir-name (string-append dir-name "-res-" (number->string resolution) ))
; (set! dir-name (string-append dir-name "-geom-" (number->string geometry-num) ))
; (use-output-directory dir-name)
; TODO----------------------------------------------------

; (run-until 1
  ; (at-beginning output-epsilon)
; )
(run-until (* 1 endt) (at-beginning output-epsilon)
           (lambda ()
             (change-sources! (list (make source
                                      (src (make continuous-src
                                             (frequency 1e-10)))
                                      (component Ex)
                                      (center 0 0 (+ (* -0.5 GRID_SIZE_Z) DPML
                                                 (* v (meep-time))))))))
           ; (at-every 2 (output-png Hz "-vZc dkbluered -M 1")) )
           (at-every 2 (output-png Ex "-0 -y 0 -vZc dkbluered -M 1 -C $EPS")) )

