; Source: https://www.mail-archive.com/meep-discuss@ab-initio.mit.edu/msg04235.html
; 

(define-param sx 40)
(define-param sy 40)
(set! geometry-lattice (make lattice (size sx sy no-size)))
(define-param dpml 1.0)
(set! pml-layers (list (make pml (thickness dpml))))
; (define-param eps 2.25)
(define-param eps 3.8)
(set! default-material (make dielectric (epsilon eps)))


(set-param! resolution 10)

(define-param v 0.8) ; point charge velocity
(set! symmetries (list (make mirror-sym (direction Y))))
(define-param endt (/ sx v))
(run-until endt (at-beginning output-epsilon)
           (lambda ()
             (change-sources! (list (make source
                                      (src (make continuous-src
                                             (frequency 1e-10)))
                                      (component Ex)
                                      (center (+ (* -0.5 sx) dpml
                                                 (* v (meep-time))))))))
           ; (at-every 2 (output-png Hz "-vZc dkbluered -M 1")) )
           (at-every 2 (output-png Ex "-vZc dkbluered -M 1")) )