(define DPML 1)
(define fcen 0.25 )
(define df 0.01)

(set! geometry-lattice (make lattice (size 10 10 no-size)))
; surrounding material: air
(set! default-material air )

(set! geometry (list )
)

(set! resolution 15)
(set! pml-layers (list (make pml (thickness DPML))))

(define (reposition vec )
  ; (make-polar 1 (/ (* -2 pi) 3 ) )
  ; (define srcPosition (vector3*  (* 0.03 (meep-time))  (vector3 1 0 0 )   ) )
  (begin
  (print vec ",\n" )
  ; (make-polar 1 (* (* 2 pi) (meep-round-time) fcen ) )
  (make-polar 1 0)
  )
  
  ; (if (vector3-close? vec srcPosition 1e-3)
    ; 1 0
  ; )   
)
  
(define (const-src-func t) (make-rectangular 1.0 0 ) )

; (define src-pos-z (* 0.34 longitudinal-constant))

(define srcList (list
               (make source
                 (src 
                  ; (make custom-src 
                    ; (src-func const-src-func)
                  ; )
                  (make gaussian-src
                     (frequency fcen)
                     (fwidth df)
                   )
                   ; (make continuous-src (frequency fcen))
                 )
                 (component Ez)
                 (center 1 0 )
                 ; (amp-func reposition)
                 )                
  ) 
)

(define (time-has-passed) 
  (if (>= (meep-round-time) 20) #t #f)
 )

(set! sources srcList)
; custom-src

(do ((i 1 (1+ i)))
    ((> i 10))
  (run-until 1
  ; (in-volume (volume (center 0) (size (* 2 grid-size-r)))
    (at-beginning output-epsilon)
    (to-appended (string-append "ez-" (number->string i) ) (at-every (/ 1 fcen 10) output-efield-z))
  ; )
  )
  ; phi = pi
  ; T = 4 = 1 / fcen
  ; T_sim = 11
  ; T_sim % T = 2
  ; 2/4 = pi/2pi

  ; T = 4 = 1 / fcen
  ; T_sim = 11
  ; T_sim % T = 3
  ; 3/4 = phi/2pi
  ; phi = 3/2 pi

  ; T = 3
  ; T_sim = 10
  ; 10 %3 = 1
  ; phi = 2/3 pi 
  (change-sources! (list
                 (make source
                   (src 
                    ; (make custom-src 
                      ; (src-func const-src-func)
                    ; )
                    (make gaussian-src
                       (frequency fcen)
                       (fwidth df)
                     )
                     ; (make continuous-src (frequency fcen))
                   )
                   (component Ez)
                   (center (+ 1 (* 0.1 (meep-round-time) ) ) 0 )
                   (amp-func reposition)
                   )                
    )
  )
)
  
; (run-until 11
  ; ; (in-volume (volume (center 0) (size (* 2 grid-size-r)))
    ; (at-beginning output-epsilon)
    ; (to-appended "ez-1" (at-every (/ 1 fcen 10) output-efield-z))
  ; ; )
; )
; ; phi = pi
; ; T = 4 = 1 / fcen
; ; T_sim = 11
; ; T_sim % T = 2
; ; 2/4 = pi/2pi

; ; T = 4 = 1 / fcen
; ; T_sim = 11
; ; T_sim % T = 3
; ; 3/4 = phi/2pi
; ; phi = 3/2 pi

; ; T = 3
; ; T_sim = 10
; ; 10 %3 = 1
; ; phi = 2/3 pi 

; (change-sources! (list
               ; (make source
                 ; (src 
                  ; ; (make custom-src 
                    ; ; (src-func const-src-func)
                  ; ; )
                  ; ; (make gaussian-src
                     ; ; (frequency fcen)
                     ; ; (fwidth df)
                   ; ; )
                   ; (make continuous-src (frequency fcen))
                 ; )
                 ; (component Ez)
                 ; (center (+ 1 (* 0.1 (meep-round-time) ) ) 0 )
                 ; (amp-func reposition)
                 ; )                
  ; )
; )

; (run-until (lambda () (>= (meep-round-time) 22 ) ) 
    ; ; (at-beginning output-epsilon)
    ; (to-appended "ez-2" (at-every (/ 1 fcen 10) output-efield-z))
; )

; ;-------------------------------------
; (set! interactive? false)
; (init-fields)
; ; do stuff

; (define (run-until cond? . step-funcs)

  ; (set! interactive? false)

  ; (if (null? fields) (init-fields))

  ; (if (number? cond?) ; cond? is a time to run for

      ; (let ((T0 (meep-round-time))) ; current Meep time
        ; (apply run-until 
          ; (cons 
            ; (lambda () (>= (meep-round-time) (+ T0 cond?) )
            ; )
            ; (cons 
              ; (display-progress T0 (+ T0 cond?) progress-interval) step-funcs
            ; )
          ; )
        ; )
      ; )

      ; (begin ; otherwise, cond? is a boolean thunk

	; (map (lambda (f) (eval-step-func f 'step)) step-funcs)

	; (if (cond?)
	    ; (begin
	      ; (map (lambda (f) (eval-step-func f 'finish)) step-funcs)
	      ; (print "run " run-index " finished at t = " (meep-time)

		     ; " (" (meep-fields-t-get fields) " timesteps)\n")

	      ; (set! run-index (+ run-index 1)))

	    ; (begin

	      ; (meep-fields-step fields)

	      ; (apply run-until (cons cond? step-funcs)))))
  ; )
; )








