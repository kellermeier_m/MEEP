# -*- coding: utf-8 -*-
"""
Created on May 09 2017

@author: maxke
"""
from __future__ import print_function
import numpy as np
from matplotlib import pyplot as plt
import matplotlib.ticker as plticker
#from subprocess import call
# import time
import os
import re
import sys


def run_meep_excite(k_z=0., r=None, dir_path='varied_k_z'):
    '''
    This functions runs MPB for a effectively two dimensional crystal of
    hexagonally aranged cylindrical rods of glass.

    k_z is the out-of-plane vector in whose direction the translational symmetry is continuous,
        not discrete as in the plane.
    r is the radius of the cylinders
    '''
    # os.chdir("2d-hexagonal")
    # if r is not None:
        # cmd_mpb = "mpb K_Z={0:.3f} RADIUS={1:.3f} glass-hexagonal-cyl-bands.ctl | tee {2}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, r, dir_path)
    # else:
        # cmd_mpb = "mpb K_Z={0:.3f} glass-hexagonal-cyl-bands.ctl | tee {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out".format(k_z, dir_path)
    cmd_meep = "mpirun -np 4 meep-mpi eps-given?=true excite-mode?=true initial-src-center?=true fcen={0:.3f} df=0.005 excite-mounted_2D_wvg.ctl | tee excite-mounted_2D_wvg-out/excite-mounted_2D_wvg.out "
    print("TO BE EXECUTED:", cmd_meep)
    os.system(cmd_meep)
    
    # After MPB has finished
    # grep freq glass/bands-cuboid-rod-cell-ratio-0.33.out > glass/bands-cuboid-rod-cell-ratio-0.33.freq.dat
    cmd_grep= "grep freq {1}/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.out > varied_k_z/glass-hexagonal-cyl-bands-k_z-{0:0.3f}.freq.dat".format(k_z, dir_path)
    print("TO BE EXECUTED:", cmd_grep)
    os.system(cmd_grep)
    os.chdir("..")

def generate_band_files(numBands = 8,  dir_path='varied_k_z'):
    '''
    For each band returned from MPB a file is created containing all frequencies
        depending on the wavevector.
    '''
    current_band = 1

    os.chdir("2d-hexagonal/{0}".format(dir_path))
    for file in os.listdir('.'):
        if file.endswith(".freq.dat"):
            # scheme of the files:
            # freqs:, k index, kx, ky, kz, kmag/2pi, band 1, band 2, band 3, band 4, band 5, band 6, band 7, band 8
            mpb_output = np.genfromtxt(file, skip_header=1, delimiter=",", dtype="str")

            for i in range(numBands):
                with open('{0}.band.dat'.format(i+1), "ab") as f:
                # write kx, ky, kz, band i
                    # print(mpb_output[ :, [2,3,4, 6+i] ].dtype)
                    np.savetxt(f, mpb_output[ :, [2,3,4, 6+i] ], fmt='%s', delimiter=",")

            # print(os.path.join("/mydir", file))

    os.chdir('../..')

    # tm_bands_import= np.genfromtxt(path_freq_output, skip_header=1, delimiter=",", dtype="str")
    # tm_bands_data = tm_bands_import[:,1:].astype(float)

def read_all_resonant_modes(filename):
    with open(filename, "r") as file:    
        freqs_w_header= [ (line.split(","))[1] for line in file if re.search("harminv", line)] 
        print(freqs_w_header)
        freqs= np.array([value for value in freqs_w_header if value != " frequency"], dtype='float')    
    return freqs


if __name__ == '__main__':
    
        # for line in file:
            # if re.search("harminv", line):
                # print(line)
                # print((line.split(","))[1])
                # print(linesplit[1])


