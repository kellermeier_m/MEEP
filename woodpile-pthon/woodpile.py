# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 09:45:48 2017

@author: maxke
"""

from meep_mpi import *
from woodpile_params import *


class rod_polygon():
    def __init__(self,pos_x, pos_y):
        '''
        This class defines the polygon (2d) to describe a rod in a stack
        '''
        self._x = pos_x
        self._y = pos_y
        pol1_points=numpy.zeros((5,2))
        pol1_points[0] = [PAD,PAD]    # start point
        pol1_points[1] = [PAD+RODLENGTH, PAD]
        pol1_points[2] = [PAD + RODLENGTH, PAD + RODWIDTH]
        pol1_points[3] = [PAD, PAD +  RODWIDTH]
        pol1_points[4] = [PAD,PAD]    # end point = start point
        pol1_points = pol1_points + [pos_x, pos_y] 
        self.polygon = pol1_points

        
#        self.add_polygon(pol1_points,1)
        
    def rotate():
        pass


class rod(PolygonCallback3D):
    def __init__(self):
        PolygonCallback3D.__init__(self)
        master_printf("Callback function for epsilon contructed.\n")
        master_printf("Now defining the matrial stacks...\n")
        ms= numpy.zeros([4,4])
        #first stack
        # id, thickness, epsilon, number of lines in stack
        ms[0] = [1, RODHEIGHT, EPSILON, 2]
        ms[1] = [1, RODHEIGHT, 1., 2]
        #second stack
        ms[2] = [2, RODHEIGHT, 1.,1]
        ms[3] = [3, RODHEIGHT, EPSILON,1]
        # third stack
        
        self.add_material_stacks_from_numpy_matrix(ms, 3)
        master_printf("Now defining the polygons...\n")
        
        # polygon 1 in stack 1
#        first_rod = rod_polygon(0.,0.) 
        
        for i in range(int(NUM_RODS)):
            self.add_polygon(rod_polygon(0., i*DISTANCE).polygon, 1 )    
#        self.add_polygon(first_rod.polygon, 1)
        
        
        pol2_points=numpy.zeros((5,2))
        pol2_points[0] = [0.0, 0.0]    # start point
        pol2_points[1] = [0., GRID_SIZE_Y]
        pol2_points[2] = [GRID_SIZE_X, GRID_SIZE_Y]
        pol2_points[3] = [GRID_SIZE_X, 0]
        pol2_points[4] = [0.0, 0.]  # end point = start point
        self.add_polygon(pol2_points,2)
        
        pol1_points=numpy.zeros((5,2))
        pol1_points[0] = [PAD,PAD]    # start point
        pol1_points[1] = [PAD ,PAD+RODLENGTH]
        pol1_points[2] = [PAD + RODWIDTH, PAD + RODLENGTH]
        pol1_points[3] = [PAD +  RODWIDTH, PAD ]
        pol1_points[4] = [PAD,PAD]    # end point = start point
        self.add_polygon(pol1_points,3)
        
        


if __name__ == "__main__":
    comp_vol = vol3d(GRID_SIZE_X,GRID_SIZE_Y,GRID_SIZE_Z,5)
    material = rod()
    set_EPS_Callback(material.__disown__())
    use_averaging(False)
    s = structure(comp_vol, EPS, pml(0.25))
    f = fields(s)
    eps_file =  prepareHDF5File("woodpile_rod.h5")
    f.output_hdf5(Dielectric, comp_vol.surroundings(), eps_file)
#    del_EPS_Callback() 
    
    