# -*- coding: utf-8 -*-
"""
Created on Tue Apr  4 09:47:14 2017

@author: maxke
"""


RODHEIGHT= 4.
RODWIDTH= 4.
DISTANCE= 9.     # distance between the rods in a layer, measured from center to center

VERT_PERIOD = 4*RODHEIGHT
NUM_RODS = 4.  # number of rods in the base plane

# Two different rod lengths are neccessary
RODLENGTH= ( NUM_RODS - 1. ) * DISTANCE + RODWIDTH
NUM_RODS_SECOND= NUM_RODS -1.    # number of rods in second half period, due to uverhang of rod
NUM_VERTICAL= 1.         # number of periods in Vertical direction

#Anzahl der Reihen, die in vertikale Richtung aus dem Stack geschnitten wird + ein halber Balken aufgrund der Symmetriebrechung
NUM_HOLEHEIGHT = 3
HOLE_HALFHEIGHT =  NUM_HOLEHEIGHT* RODHEIGHT + RODHEIGHT/ 2.

#Anzahl der Reihen, die in horizontale Richtung aus dem Stack geschnitten wird
NUM_HOLEWIDTH = 1
HOLE_HALFWIDTH = ( DISTANCE - RODWIDTH) / 2. + DISTANCE * NUM_HOLEWIDTH

# Material
EPSILON = 12.1  #Silicon
  
# Simulation settings
PAD= 1.
GRID_SIZE_X = RODLENGTH + 2*PAD
GRID_SIZE_Y = RODLENGTH + 2*PAD
GRID_SIZE_Z = 10

