(define-param PAD 1.) ; padding from wall

; Geometry parameters
; Rod dimensions
(define-param RODHEIGHT 1.)
(define-param RODWIDTH 1.)
; Distance between rods in x resp. y direction
(define-param DISTANCE 4.)
; number of rods in the base plane
(define-param NUM_RODS 5.)  
; distance of translational invariance of the structure in z direction
(define-param VERT_PERIOD (* 4 RODHEIGHT))
; TODO: Two different rod lengths are neccessary in FUTURE 
(define-param RODLENGTH (+ (* (- NUM_RODS 1. ) DISTANCE) RODWIDTH)) ; ( NUM_RODS - 1. ) * DISTANCE + RODWIDTH
; (set-param! RODLENGTH 14)
; number of rods in second half period, due to overhang of rod
(define-param NUM_RODS_SECOND (- NUM_RODS 1.)) 
(define-param NUM_VERTICAL 1.)   ;  number of periods in Vertical direction

;Anzahl der Reihen, die in vertikale Richtung aus dem Stack geschnitten wird + ein halber Balken aufgrund der Symmetriebrechung
(define-param NUM_HOLEHEIGHT 3)
(define-param HOLE_HALFHEIGHT (+ (* NUM_HOLEHEIGHT RODHEIGHT) (/ RODHEIGHT 2.))  )

;Anzahl der Reihen, die in horizontale Richtung aus dem Stack geschnitten wird
(define-param NUM_HOLEWIDTH  1)
(define-param HOLE_HALFWIDTH 
    ( + 
        ( / (- DISTANCE RODWIDTH) 2.) 
        ( * DISTANCE NUM_HOLEWIDTH) 
    )
)

    
; Material
(define-param EPSILON 12.1) ; Silicon

; (define-param GRID_SIZE_X 16) ; RODLENGTH + 2*PAD
; (define-param GRID_SIZE_Y 16) ; = RODLENGTH + 2*PAD
(define-param GRID_SIZE_Z 16) ; = 10)

(define-param GRID_SIZE_X  (+ RODLENGTH (* 2 PAD))) ; RODLENGTH + 2*PAD
(define-param GRID_SIZE_Y (+ RODLENGTH (* 2 PAD)) ) ; = RODLENGTH + 2*PAD
(define-param GRID_SIZE_Z 16) ; = 10)
(define-param GRID_SIZE_Z (+ (* 2 VERT_PERIOD NUM_VERTICAL) (* 2 PAD) )); = 10)

; Check that the dimensions of the structure fit into this grid!



; ----------------------------------------------------------------------------

(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material (make dielectric (epsilon 1)) ) 

;geometry
(define geometryList (list ) )    


    ; first layer
    ; REMARK: The z coordinate of the center is negative because the negative half cube is the symmetry 
    ;           defining half cube. If the rods are placed in the upper half cube they will be erased 
    ;           by exploiting the symmetry
    (do ((i 0 (1+ i)))          ; ((variable initialValue step) …) 
        ( (>= i NUM_RODS  ) )      ; Condition, Here: "until"  i >= NUM_RODS
        ; TODO
        (set! geometryList (append geometryList 
                (list 
                (make block 
                    ( center 0 
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE) )  
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE
                        0)
                    ( size RODLENGTH RODWIDTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
                ; other rods will follow
            )
        )
    )
    ; second layer
    (do ((i 0 (1+ i)))
        ( (>= i NUM_RODS  ) )
        ; loop block
        (set! geometryList (append geometryList 
                (list 
                (make block 
                    ( center 
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE) )  
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE
                        0
                        (* -1 RODHEIGHT) )
                    ( size RODWIDTH RODLENGTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
                ; other rods will follow
            )
        )
    )

    ;third layer
    (do ((i 0 (1+ i)))
        ( (>= i NUM_RODS_SECOND  ) )
        (set! geometryList (append geometryList 
                (list 
                (make block 
                    ( center 
                        0 
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE)  (* DISTANCE 0.5 ))  
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE - DISTANCE/2
                        (* -1 (* 2 RODHEIGHT) )
                        )
                    ( size RODLENGTH RODWIDTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
            )
        )
    )

    ;fourth layer
    (do ((i 0 (1+ i)))
        ( (>= i NUM_RODS_SECOND  ) )
        ; loop block
        (set! geometryList (append geometryList 
                (list 
                (make block 
                    ( center 
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE)  (* DISTANCE 0.5 ) )  
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE
                        0
                        (* -1 (* 3 RODHEIGHT) )
                        )
                    ( size RODWIDTH RODLENGTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
            )
        )
    )





(set! geometry 
    geometryList
)

(set! symmetries (list (make mirror-sym (direction Z) (phase -1) )))

    
(set! pml-layers (list (make pml (thickness 1.0))))
(set! resolution 10)    

; Simulation settings
(run-until 2
           (at-beginning output-epsilon)
           (at-end output-efield-z))
