(define-param PAD 1.) ; padding from wall

; Geometry parameters
; Cell parameters
; Distance between rods in x resp. y direction
(define-param DISTANCE 1)  ; corresponds to 'a'  in Cowan, B.
; distance of translational invariance of the structure in z direction
(define-param VERTICAL_PERIOD (* DISTANCE (sqrt 2)) ); corresponds to 'c'  in Cowan, B.


; Rod dimensions
(define RODWIDTH (* 0.25 DISTANCE) )
(define RODHEIGHT (* VERTICAL_PERIOD 0.25 ) )
; TODO: Two different rod lengths are neccessary in FUTURE

; number of rods in the base plane
(define-param NUM_RODS 7.)
; number of rods in second half period, due to overhang of rod
(define-param NUM_RODS_SECOND (- NUM_RODS 1.))
(define-param NUM_VERTICAL 2.)   ;  number of periods in Vertical direction

(define RODLENGTH (+ (* (- NUM_RODS 1. ) DISTANCE) RODWIDTH)) ; ( NUM_RODS - 1. ) * DISTANCE + RODWIDTH


;Anzahl der Reihen, die in vertikale Richtung aus dem Stack geschnitten wird + ein halber Balken aufgrund der Symmetriebrechung
(define-param NUM_HOLEHEIGHT 3)
(define-param HOLE_HALFHEIGHT (+ (* NUM_HOLEHEIGHT RODHEIGHT) (/ RODHEIGHT 2.))  )
;Anzahl der Reihen, die in horizontale Richtung aus dem Stack geschnitten wird
(define-param NUM_HOLEWIDTH  1)
(define-param HOLE_HALFWIDTH
    ( +
        ( / (- DISTANCE RODWIDTH) 2.)
        ( * DISTANCE NUM_HOLEWIDTH)
    )
)
; Material
(define-param EPSILON 12.1) ; Silicon
(define-param EPSILON (* 1.95 1.95)) ; Glass @ THz
(define Glass (make dielectric (epsilon EPSILON)))
(define Silicon (make dielectric (epsilon 12.1)))


(define-param GRID_SIZE_X  (+ RODLENGTH (* 2 PAD))) ; RODLENGTH + 2*PAD
(define-param GRID_SIZE_Y (+ RODLENGTH (* 2 PAD)) ) ; = RODLENGTH + 2*PAD
(define-param GRID_SIZE_Z (+ (* 2 VERTICAL_PERIOD NUM_VERTICAL) (* 2 PAD) ) ) ; = 10)
(define-param DPML PAD)  ; thickness of the pml layer

; Source parameters
(define-param fcen 0.15) ; pulse center frequency
(define-param df 0.01)  ; pulse width (in frequency)

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))

(set! epsilon-input-file "woodpile_stack-out/eps-000000.00.h5" )

;(set! symmetries (list (make mirror-sym (direction Z) (phase -1) )))


(set! pml-layers (list (make pml (thickness DPML))))
(set! resolution 10)

; TODO: Define source
(set! sources (list
               (make source
                 (src (make gaussian-src (frequency fcen) (fwidth df)))
                 (component Ez)
                 (center
                   0
                   0
                   0
                   )
               )
              )
)


(use-output-directory)
; Simulation settings
(run-until (/ 1 fcen)
  (at-beginning output-epsilon)
  (at-every (/ 1 fcen 20) output-efield-z)
  )




;(run-until 2
;           (at-beginning output-epsilon)
;           (at-end output-efield-z))
