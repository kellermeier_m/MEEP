(define-param PAD 1.) ; padding from wall

; Geometry parameters
; Cell parameters
; Distance between rods in x resp. y direction
(define-param DISTANCE 1)  ; corresponds to 'a'  in Cowan, B.
; distance of translational invariance of the structure in z direction
(define-param VERTICAL_PERIOD (* DISTANCE (sqrt 2)) ); corresponds to 'c'  in Cowan, B.


; Rod dimensions
(define RODWIDTH (* 0.25 DISTANCE) )
(define RODHEIGHT (* VERTICAL_PERIOD 0.25 ) )
; TODO: Two different rod lengths are neccessary in FUTURE
(define RODLENGTH (+ (* (- NUM_RODS 1. ) DISTANCE) RODWIDTH)) ; ( NUM_RODS - 1. ) * DISTANCE + RODWIDTH

; number of rods in the base plane
(define-param NUM_RODS 7.)
; number of rods in second half period, due to overhang of rod
(define-param NUM_RODS_SECOND (- NUM_RODS 1.))
(define-param NUM_VERTICAL 2.)   ;  number of periods in Vertical direction

;Anzahl der Reihen, die in vertikale Richtung aus dem Stack geschnitten wird + ein halber Balken aufgrund der Symmetriebrechung
(define-param NUM_HOLEHEIGHT 3)
(define-param HOLE_HALFHEIGHT (+ (* NUM_HOLEHEIGHT RODHEIGHT) (/ RODHEIGHT 2.))  )
;Anzahl der Reihen, die in horizontale Richtung aus dem Stack geschnitten wird
(define-param NUM_HOLEWIDTH  1)
(define-param HOLE_HALFWIDTH
    ( +
        ( / (- DISTANCE RODWIDTH) 2.)
        ( * DISTANCE NUM_HOLEWIDTH)
    )
)
; Material
(define-param EPSILON 12.1) ; Silicon
(define-param EPSILON (* 1.95 1.95)) ; Glass @ THz
(define Glass (make dielectric (epsilon EPSILON)))
(define Silicon (make dielectric (epsilon 12.1)))

; Synchronisity condition
(define-param BETA 0.25)  ; not used yet

(define-param GRID_SIZE_X  (+ RODLENGTH (* 2 PAD))) ; RODLENGTH + 2*PAD
(define-param GRID_SIZE_Y (+ RODLENGTH (* 2 PAD)) ) ; = RODLENGTH + 2*PAD
(define-param GRID_SIZE_Z (+ (* 2 VERTICAL_PERIOD NUM_VERTICAL) (* 2 PAD) ) ) ; = 10)
(define-param DPML PAD)  ; thickness of the pml layer

; Source parameters
(define-param fcen 0.3) ; pulse center frequency
(define-param df 1.5)  ; pulse width (in frequency)

; ----------------------------------------------------------------------------
(set! geometry-lattice (make lattice (size GRID_SIZE_X GRID_SIZE_Y GRID_SIZE_Z)))
; surrounding material: air
(set! default-material (make dielectric (epsilon 1)) )

;geometry
(define geometryList (list  ) )

(do ((j 0 (1+ j)) )
    ( (>= j NUM_VERTICAL  ) )
    ; first layer
    ; REMARK: The z coordinate of the center is negative because the negative half cube is the symmetry
    ;           defining half cube. If the rods are placed in the upper half cube they will be erased
    ;           by exploiting the symmetry
    (do ((i 0 (1+ i)))          ; ((variable initialValue step) …)
        ( (>= i NUM_RODS  ) )      ; Condition, Here: "until"  i >= NUM_RODS
        ; TODO
        (set! geometryList (append geometryList
                (list
                (make block
                    ( center 0
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE) )
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE
                        (* -1 j VERT_PERIOD) )
                    ( size RODLENGTH RODWIDTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
            )
        )
    )
    ; second layer
    (do ((i 0 (1+ i)))
        ( (>= i NUM_RODS  ) )
        ; loop block
        (set! geometryList (append geometryList
                (list
                (make block
                    ( center
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE) )
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE
                        0
                        (* -1 (+ RODHEIGHT  (* j VERT_PERIOD) ) )
                    )
                    ( size RODWIDTH RODLENGTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
                ; other rods will follow
            )
        )
    )

    ;third layer
    (do ((i 0 (1+ i)))
        ( (>= i NUM_RODS_SECOND  ) )
        (set! geometryList (append geometryList
                (list
                (make block
                    ( center
                        0
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE)  (* DISTANCE 0.5 ))
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE - DISTANCE/2
                        (* -1 (+ (* 2 RODHEIGHT)  (* j VERT_PERIOD) ) )
                            ; -(2 * RODHEIGHT + j* VERT_PERIOD)
                    )
                    ( size RODLENGTH RODWIDTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
            )
        )
    )

    ;fourth layer
    (do ((i 0 (1+ i)))
        ( (>= i NUM_RODS_SECOND  ) )
        ; loop block
        (set! geometryList (append geometryList
                (list
                (make block
                    ( center
                        (- (/ GRID_SIZE_Y 2.) PAD (/ RODWIDTH 2)   (* i DISTANCE)  (* DISTANCE 0.5 ) )
                            ; GRID_SIZE_Y/2 - PAD - RODWIDTH/ 2 - i * DISTANCE
                        0
                        (* -1 (+ (* 3 RODHEIGHT)  (* j VERT_PERIOD) ) )
                        )
                    ( size RODWIDTH RODLENGTH RODHEIGHT)
                    (material (make dielectric (epsilon EPSILON)))
                )
                )
            )
        )
    )
)

; Define the waveguide hole in transverse plane
(define waveguide (make block
    ( center 0 0 0 )
    ( size GRID_SIZE_X (* 2 HOLE_HALFWIDTH) (* 2 HOLE_HALFHEIGHT))
    (material air)
))

(set! geometry
    (append geometryList (list waveguide) )
)

(set! symmetries (list (make mirror-sym (direction Z) (phase -1) )))


(set! pml-layers (list (make pml (thickness 1.0))))
(set! resolution 10)

; TODO: Define source
(set! sources (list
               (make source
                 (src (make gaussian-src (frequency fcen) (fwidth df)))
                 (component Ex)
                 (center
                   0.1234   ; try random
                   0.1930   ; try random
                   0
                   )
               )
              )
)


(use-output-directory)
; Simulation settings
(run-sources+ 300
    (at-beginning output-epsilon)
    (after-sources (harminv Ex (vector3 0 0 0 ) fcen df
        )))









;(run-until 2
;           (at-beginning output-epsilon)
;           (at-end output-efield-z))
